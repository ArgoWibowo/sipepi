<?php 
session_start();

$database = new mysqli("localhost","root","","sipepi");

define("BASE_URL","http://localhost/sipepi/admin");

class tanah
{
	public $koneksi;
	function __construct($mysqli)
	{
		$this->koneksi = $mysqli;
	}

	function simpan_tanah($Des_singkat, $Des_lengkap, $Foto_tanah, $Alamat_tanah, $Kota, $Harga){
		$namafoto = $Foto_tanah["name"];
		$lokasifoto = $Foto_tanah["tmp_name"];

		move_uploaded_file($lokasifoto, "img/$namafoto");

		$this->koneksi->query("INSERT INTO tanah (Des_singkat, Des_lengkap, Foto_tanah,
			Alamat_tanah, Kota, Harga) VALUES ('$Des_singkat','$Des_lengkap','$namafoto',
			'$Alamat_tanah','$Kota','$Harga')") or die(mysqli_error($this->koneksi));
		return 'sukses';
	}

	function display_all_tanah(){
		$semuatanah = array();
		$ambiltanah = $this->koneksi->query("SELECT * FROM tanah");
		while($gettanah = $ambiltanah->fetch_assoc())
		{
			$semuatanah[]= $gettanah;
		}
		return $semuatanah;
	}

	function display_tanah_by_id($id){
		$semuatanah = array();
		$ambiltanah = $this->koneksi->query("SELECT * FROM tanah where Id_tanah = $id");
		while($gettanah = $ambiltanah->fetch_assoc())
		{
			$semuatanah[]= $gettanah;
		}
		return $semuatanah;
	}

	function hapus_tanah($id){
		$this->koneksi->query("DELETE FROM tanah WHERE Id_tanah='$id'");
	}

	function update_tanah($Id_tanah, $Des_singkat, $Des_lengkap, $Foto_tanah, $Alamat_tanah, $Kota, $Harga){
		//cek foto apakah ada update atau tidak
		if($Foto_tanah["name"]!=''){
			$namafoto = $Foto_tanah["name"];
			$lokasifoto = $Foto_tanah["tmp_name"];

			move_uploaded_file($lokasifoto, "img/$namafoto");

			$this->koneksi->query("UPDATE tanah set Des_singkat=$Des_singkat, Des_lengkap=$Des_lengkap,
				Foto_tanah=$namafoto, Alamat_tanah=$Alamat_tanah, Kota=$Kota, Harga=$Harga WHERE Id_tanah=$Id_tanah");
		}
		else{
			$this->koneksi->query("UPDATE tanah set Des_singkat='$Des_singkat', Des_lengkap='$Des_lengkap',
				Alamat_tanah='$Alamat_tanah', Kota='$Kota', Harga='$Harga' WHERE Id_tanah='$Id_tanah'");
		}
		return 'sukses';
	}
}
$tanah = new tanah($database);
?>